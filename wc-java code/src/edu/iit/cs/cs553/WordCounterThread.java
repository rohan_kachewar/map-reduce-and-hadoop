package edu.iit.cs.cs553;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WordCounterThread extends Thread {

    private File inputFile;
    private String output;
    private int id1;

    public WordCounterThread(File inputFile, String output, int id) {
        System.out.println("-----------------Starting thread id" + id);
        id1 = id;
        this.inputFile = inputFile;
        this.output = output;
    }

    @Override
    public void run() {
        try {
            System.out.println("-----------------Running thread id" + id1);
            Map<String, Integer> mp = new HashMap<String, Integer>();
            FileReader fr = new FileReader(inputFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                processLine(line, mp);
            }
            br.close();
            fr.close();

            WordCountUtil.writeMap(mp, this.output + "/" + this.inputFile.getName() + "_result.txt");
        } catch (java.io.IOException e) {
            Logger.getLogger(WordCountJ.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void processLine(String line, Map<String, Integer> map) {
        StringTokenizer st = WordCountUtil.getWordTokenizer(line);

        String[] resultString;
        while (st.hasMoreTokens()) {
            String sample = st.nextToken();            
            if(((sample.contains("'")) || (sample.contains("--")))){
                //Check for within word ' or --

                //replace all starting and ending ' and -
                //within strings by null as we will be skipping these in between words
                sample = sample.replaceAll("^('+)", "");
                sample = sample.replaceAll("('+)$", "");
                sample = sample.replaceAll("^(-+)", "");
                sample = sample.replaceAll("(-+)$", "");                

                
                resultString = sample.split("[^\\p{L}\\p{N}-']");                
            } else {
                //if no ' or --
                resultString = sample.split("[^\\p{L}\\p{N}]");
            }

            for (int i=0; i<resultString.length; i++)
            {
                WordCountUtil.addWord(map, resultString[i]);                
            }

        }
    }
}
