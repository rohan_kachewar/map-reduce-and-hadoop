Running WordCount Hadoop Map-Reduce Program

1) Place the jar wc.jar at any location

2) Then execute the jar wc.jar specifying the input and output directories. If /input is the the input directory (say) and /output is the output directory
(say) then
    Execute command bin/hadoop jar wc.jar edu.iit.cs.cs553.WordCountMR /input /output