# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Hadoop Map Reduce for word count program
* Version: 1.0


### How do I get set up? ###
* Tasks Done:
1. Java Wordcount program and its performance measurement, 
2. Setting environment for hadoop using VMs, 
3. Hadoop Wordcount program, 
4. Taking performance readings for Hadoop


*Configuration:
*A) Setting up VM
We used VirtualBox for setting up VMs.
*The core machine configuration used is:
*OS: Windows XP
*Processor: Intel Core 2 Duo CPU 2.10 GHz
*RAM: 2048 MB DDR3
*Disk: HDD
*Configuration steps for VM:
1) Downloaded the Virtual Box version 4.2.6
2) Downloaded the ubuntu 12.04 version iso
3) Installed Virtual Box on the host machine
4) Installed ubuntu in the VM using iso image
5) Installed java on VM using “sudo apt-get install openjdk-7.0-jdk”
6) Installed ssh service on VM using “sudo apt-get install openssh”
*VM configuration:
*RAM: 512 MB
*Disk: 24 GB
*OS: Ubuntu 12.04