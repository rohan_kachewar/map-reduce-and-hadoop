package edu.iit.cs.cs553;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

// Author : Amey Kulkarni
// CWID: A20279557 

public class WordCountMR {

    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            String line = value.toString();
            String[] resultString;
            StringTokenizer tokenizer = WordCountUtil.getWordTokenizer(line);
            while (tokenizer.hasMoreTokens()) 
            {
                String sample = tokenizer.nextToken();            
            if(((sample.contains("'")) || (sample.contains("--")))){
                //Check for within word ' or --

                //replace all starting and ending ' and -
                //within strings by null as we will be skipping these in between words
                sample = sample.replaceAll("^('+)", "");
                sample = sample.replaceAll("('+)$", "");
                sample = sample.replaceAll("^(-+)", "");
                sample = sample.replaceAll("(-+)$", "");
                System.out.println(sample);

                
                resultString = sample.split("[^\\p{L}\\p{N}-']");                
            } else 
            {
                //if no ' or --
                resultString = sample.split("[^\\p{L}\\p{N}]");
            }

            for (int i=0; i<resultString.length; i++)
            {
                word.set(resultString[i]);                
            }
                //word.set(tokenizer.nextToken());
                output.collect(word, one);
            }
        }
    }

    public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            int sum = 0;
            while (values.hasNext()) {
                sum += values.next().get();
            }
            output.collect(key, new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws IOException {
        JobConf conf = new JobConf(WordCountMR.class);
        conf.setJobName("wordcount");

        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);

        conf.setMapperClass(Map.class);
        conf.setCombinerClass(Reduce.class);
        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[1]));
        FileOutputFormat.setOutputPath(conf, new Path(args[2]));

        JobClient.runJob(conf);
    }
}
